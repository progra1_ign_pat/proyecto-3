#include <stdio.h>
#include <stdlib.h>

typedef struct{ 
	int nem, rank, leng, mat, hist, cs, bea, cupospsu,pond,psu;
	float max, min;
	//estructura donde se ve puntajes y cupos
}ponderaciones;

typedef struct{
	int codigo;	
	char carrera[100];
	ponderaciones ponderacion;
	//estructura donde se almacena el nombre de la carrera, codigo y otra estructura donde contendra informacion adicional de esta
}admision;
//prototipos
void cargar_arch(admision lista[]);
void caso1(int op,admision lista[]);
void caso2(int cod,admision lista[]);
void caso3(int op,admision lista[]);

int main(){
	char opc;
	int facultad,cod;
	admision lista[53];
	cargar_arch(lista);
	//funcion main: se carga la lista de carreras en arreglos.
	do{
		printf("\nIngrese el numero de lo que desea hacer\n(1)Consultar ponderacion carrera\n(2)Simular Postulacion Carrera\n(3)Mostrar Ponderaciones Facultad\n(4)Salir\n");
		scanf("%s",&opc);
		//se pregunta la opcion y se hace un switch para hacer estructura selectiva.
		switch(opc){
			case '1':
				//aqui es donde se consulta la ponderacion de la carrera, primero preguntando en que facultad.
				printf("\nIngrese la facultad: \n");
				printf("(1)Facultad de Arquitectura\n");
				printf("(2)Facultad de Ciencias\n");
				printf("(3)Facultad de Ciencias del mar y de recursos naturales\n");
				printf("(4)Facultad de Ciencias economicas y administrativas\n");
				printf("(5)Facultad de Derecho y Ciencias sociales\n");
				printf("(6)Facultad de Farmacia\n");
				printf("(7)Facultad de Humanidades\n");
				printf("(8)Facultad de Ingenieria\n");
				printf("(9)Facultad de Medicina\n");
				printf("(10)Facultad de Odontologia\nOpcion: ");
				//luego de scanf se manda todo a la funcion caso 1
				scanf("%d",&facultad);
				printf("\n");
				caso1(facultad,lista);				
				break;
			case '2':
				//pide el codigo antes visto en caso 1 y lo manda a caso 2
				printf("Ingrese codigo de la carrera a simular: ");
				scanf("%d",&cod);
				caso2(cod,lista);
				break;

			case '3':
			//lo mismo que caso 1
				printf("\nIngrese la facultad: \n");
				printf("(1)Facultad de Arquitectura\n");
				printf("(2)Facultad de Ciencias\n");
				printf("(3)Facultad de Ciencias del mar y de recursos naturales\n");
				printf("(4)Facultad de Ciencias economicas y administrativas\n");
				printf("(5)Facultad de Derecho y Ciencias sociales\n");
				printf("(6)Facultad de Farmacia\n");
				printf("(7)Facultad de Humanidades\n");
				printf("(8)Facultad de Ingenieria\n");
				printf("(9)Facultad de Medicina\n");
				printf("(10)Facultad de Odontologia\nOpcion: ");
				scanf("%d",&facultad);
				printf("\n");
				caso3(facultad,lista);
				break;
			case '4':
				//opcion para salir del programa
				printf("Sesion finalizada.\n");
				break;

			default:
				//en caso de ingresar mal la opcion
				printf("ERROR CARACTER INGRESADO INVALIDO\n");
				break;
		}
	}while(opc != '4');//se detiene al llegar a 4
}

void cargar_arch(admision lista[]){
	//se carga el txt de las carreras llamado ponderado.txt
	FILE *archivo;
	int i = 0,j=0;	
	archivo = fopen("ponderados.txt","r");
	if(archivo == NULL){
		printf("ERROR NO SE PUDO ABRIR EL ARCHIVO");
		//si hay error al abrir el archivo tirara el mensaje de error
	}
	else{
		while(feof(archivo) == 0){
			//se almacenan los datos en el arreglo lista que contiene en cada una la estructura admision y dentro la estructura ponderacion
			fscanf(archivo,"%s	%d	%d	%d	%d	%d	%d	%d	%d	%d	%f	%f	%d	%d",lista[i].carrera, &lista[i].codigo, &lista[i].ponderacion.nem, &lista[i].ponderacion.rank, &lista[i].ponderacion.leng, &lista[i].ponderacion.mat, &lista[i].ponderacion.hist, &lista[i].ponderacion.cs,&lista[i].ponderacion.pond,&lista[i].ponderacion.psu,&lista[i].ponderacion.max,&lista[i].ponderacion.min, &lista[i].ponderacion.cupospsu, &lista[i].ponderacion.bea );
			while(lista[i].carrera[j]!='\t'){
				if(lista[i].carrera[j]=='-'){
            		lista[i].carrera[j]=' ';}
            	else if(lista[i].carrera[j]=='_'){
            		lista[i].carrera[j]='-'; 
            		//aqui se elimina los "-" ya que el fscanf al ver un espacio llegaba hasta ahi con el string           	
        		}
        		j++;
			}
			j=0;
        /*if(lista[i].codigo==0){
			break;
		}*/
		//printf("%s %d %d %d %d %d %d %d %d %d %.1f %.1f %d %d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		
			i++;

	}
		fclose(archivo);//se cierra el archivo
	}
}

void caso1(int op,admision lista[]){
	//la funcion caso1 recorre el arreglo dependiendo la facultad seleccionada, cada if tiene un for con un i que varia
	//asi solo se reccore en la facultad de corresponde, al finalizar el recorrido pide el codigo de carrera
	//para mostrar la informacion de la carrera que se desee revisar
	int x=0,cod;
	printf("\n\nCarrera\t[Codigo de carrera]\n");
	
	if(op==1){
		for(int i=0;i<5;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}

	}
	else if(op==2){
		for(int i=5;i<9;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==3){
		for(int i=9;i<10;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==4){
		for(int i=10;i<21;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==5){
		for(int i=21;i<23;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==6){
		for(int i=23;i<25;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==7){
		for(int i=25;i<29;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==8){
		for(int i=29;i<39;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==9){
		for(int i=39;i<52;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else if(op==10){
		for(int i=52;i<53;i++){
			printf("%s		[%d]\n",lista[i].carrera, lista[i].codigo);
		}
	}
	else{
		printf("La opcion seleccionada no es valida, volviendo al menu.\n");
		x=1;
		}
	if(x==0){
		printf("\nEscribe el codigo de la carrera: ");
		scanf("%d",&cod);
		for(int i=0;i<53;i++){
			if(lista[i].codigo==cod){
				printf("\nCarrera: %s\n",lista[i].carrera);
				printf("Codigo: %d\n", lista[i].codigo );
				printf("--Ponderacion --\n");
				printf("NEM: %d\n", lista[i].ponderacion.nem );
				printf("RANK: %d\n", lista[i].ponderacion.rank);
				printf("LENG: %d\n", lista[i].ponderacion.leng);
				printf("MAT: %d\n", lista[i].ponderacion.mat);
				printf("HIST: %d\n", lista[i].ponderacion.hist);
				printf("CS: %d\n",  lista[i].ponderacion.cs);
				printf("--Ptj. MIN Postulacion--\n");
				printf("POND: %d\n",lista[i].ponderacion.pond);
				printf("PSU: %d\n",lista[i].ponderacion.psu);
				printf("--Ptj. Ponderado Primer y Ultimo Matriculado--\n");
				printf("MAX: %.1f\n",lista[i].ponderacion.max);
				printf("MIN: %.1f\n",lista[i].ponderacion.min);
				printf("--Cupos--\n");
				printf("PSU: %d\n", lista[i].ponderacion.cupospsu);
				printf("BEA: %d\n", lista[i].ponderacion.bea);

				
				x=1;
			}

		}
		if(x==0){
			printf("Codigo no encontrado.\n");
		}
	}
}
	
void caso2(int cod,admision lista[]){
	//te muestra la informacion por pantalla de la carrera el cual ingresas el codigo
	//luego te pregunta el puntaje obtenido
	//y calcula tu ponderacion para ver si tienes lo necesario para postular
	int mat,leng,histcs,nem,rank,x;
	float pormat,porleng,porhistcs,pornem,porrank,pond;
	for(int i=0;i<53;i++){
			if(lista[i].codigo==cod){
				printf("\nCarrera: %s\n",lista[i].carrera);
				printf("Codigo: %d\n", lista[i].codigo );
				printf("--Ponderacion --\n");
				printf("NEM: %d\n", lista[i].ponderacion.nem );
				printf("RANK: %d\n", lista[i].ponderacion.rank);
				printf("LENG: %d\n", lista[i].ponderacion.leng);
				printf("MAT: %d\n", lista[i].ponderacion.mat);
				printf("HIST: %d\n", lista[i].ponderacion.hist);
				printf("CS: %d\n",  lista[i].ponderacion.cs);
				printf("--Ptj. MIN Postulacion--\n");
				printf("POND: %d\n",lista[i].ponderacion.pond);
				printf("PSU: %d\n",lista[i].ponderacion.psu);
				printf("--Ptj. Ponderado Primer y Ultimo Matriculado--\n");
				printf("MAX: %.1f\n",lista[i].ponderacion.max);
				printf("MIN: %.1f\n",lista[i].ponderacion.min);
				printf("--Cupos--\n");
				printf("PSU: %d\n", lista[i].ponderacion.cupospsu);
				printf("BEA: %d\n", lista[i].ponderacion.bea);
				x=i;
			}}
	printf("\nIngrese puntaje[NEM]: ");
	scanf("%d",&nem);
	printf("\nIngrese puntaje[RANKING]: ");
	scanf("%d",&rank);
	printf("\nIngrese puntaje[MATEMATICAS]: ");
	scanf("%d",&mat);
	printf("\nIngrese puntaje[LENGUAJE]: ");
	scanf("%d",&leng);
	printf("\nIngrese puntaje[HISTORIA o CIENCIAS]: ");
	scanf("%d",&histcs);
	
	pormat=mat*(lista[x].ponderacion.mat)/100;
	porleng=leng*(lista[x].ponderacion.leng)/100;
	pornem=nem*(lista[x].ponderacion.nem)/100;
	porrank=rank*(lista[x].ponderacion.rank)/100;
	if(lista[x].ponderacion.hist==0){
		porhistcs=histcs*(lista[x].ponderacion.cs)/100;
	}
	else{
		porhistcs=histcs*(lista[x].ponderacion.hist)/100;
	}
	pond=pormat+porleng+pornem+porrank+porhistcs;
	printf("\nTu puntaje ponderado es: %.1f\n",pond);
	if(pond<lista[x].ponderacion.psu){
		printf("Muy bajo puntaje para postular.\n");
	}
	else if(pond<lista[x].ponderacion.pond || pond<lista[x].ponderacion.min){
		printf("Posible lista de espera\n");
	}
	else{
		printf("Entras al proceso de postulacion.\n");
	}
}
//Carrera/Codigo/NEM/RANK/LENG/MAT/HIST/CS/POND/PSU/MAX/MIN/PSU/BEA

void caso3(int op,admision lista[]){
	//muestra la informacion de las carreras dentro de una facultad
	//practicamente es una copia del caso 1 pero mas corto
	
	printf("\n\nCarrera / Codigo / NEM / RANK / LENG / MAT / HIST / CS / POND / PSU / MAX / MIN /CUPOS: PSU/ BEA\n");
	
	if(op==1){
		for(int i=0;i<5;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}

	}
	else if(op==2){
		for(int i=5;i<9;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==3){
		for(int i=9;i<10;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==4){
		for(int i=10;i<21;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==5){
		for(int i=21;i<23;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==6){
		for(int i=23;i<25;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==7){
		for(int i=25;i<29;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==8){
		for(int i=29;i<39;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==9){
		for(int i=39;i<52;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else if(op==10){
		for(int i=52;i<53;i++){
			printf("%s/%d/%d/%d/%d/%d/%d/%d/%d/%d/%.1f/%.1f/%d/%d\n",lista[i].carrera, lista[i].codigo, lista[i].ponderacion.nem, lista[i].ponderacion.rank, lista[i].ponderacion.leng, lista[i].ponderacion.mat, lista[i].ponderacion.hist, lista[i].ponderacion.cs,lista[i].ponderacion.pond,lista[i].ponderacion.psu,lista[i].ponderacion.max,lista[i].ponderacion.min, lista[i].ponderacion.cupospsu, lista[i].ponderacion.bea);
		}
	}
	else{
		printf("La opcion seleccionada no es valida, volviendo al menu.\n");
		
		}
	
}